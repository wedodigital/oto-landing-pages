<?php
  get_header();

  if (have_posts()) {
    while (have_posts()) {
      the_post();

      if (have_rows('content_blocks')) {
        while(have_rows('content_blocks')) {
          the_row();

          switch(get_row_layout()) {
            case 'feature_section':
              get_template_part('sections/feature-section');
              break;
            case 'logo_section':
              get_template_part('sections/logo-section');
              break;
            case 'stats_section':
              get_template_part('sections/stats-section');
              break;
            case 'review_section':
              get_template_part('sections/review-section');
              break;
            case 'image_section':
              get_template_part('sections/image-section');
              break;
            case 'icon_section':
              get_template_part('sections/icon-section');
              break;
            case 'feature_button':
              get_template_part('sections/feature-button');
              break;
            case 'footnote_section':
              get_template_part('sections/footnote-section');
              break;
          }
        }
      }
    }
  }

  get_footer();
?>
