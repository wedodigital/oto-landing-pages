<?php
	function register_my_menus() {
	  register_nav_menus(
      array(
        'mobile-menu' => __( 'Mobile Menu' ),
        'footer-menu' => __( 'Footer Menu' ),
        'footer-menu-2' => __( 'Footer Menu 2' ),
        'signposts'	=> __( 'Signposts' )
      )
	  );
	}
?>
