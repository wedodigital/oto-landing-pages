<section class="footnote-section">
  <section class="container main narrow">
    <?php the_sub_field('footnote'); ?>
  </section>
</section>
