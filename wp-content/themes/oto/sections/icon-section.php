<section class="icon-section">
  <section class="container ultra">
    <?php if(get_sub_field('icons')) { ?>
      <ul class="icon-list">
        <?php while(the_repeater_field('icons')) { ?>
          <?php
            $icon = get_sub_field('icon');
            $size = 'icon';
          ?>
          <li>
            <?php echo wp_get_attachment_image( $icon, $size ); ?>
          </li>
        <?php } ?>
      </ul>
    <?php } ?>
  </section>
</section>
