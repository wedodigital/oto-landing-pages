<?php
  $hero = get_sub_field('background_image');
  $size = 'full';
  $background = wp_get_attachment_image_src($hero, $size);
?>

<section class="image-section" style="background-image: url('<?php echo $background[0]; ?>');">
  <section class="container ultra">
    <aside class="image-section-content">
      <h2><?php the_sub_field('title'); ?></h2>
      <?php if(get_sub_field('content')) { ?>
        <?php the_sub_field('content'); ?>
      <?php } ?>
      <?php if(get_sub_field('button_url')) { ?>
        <a href="<?php the_sub_field('button_url'); ?>" class="button inverse"><?php the_sub_field('button_text'); ?></a>
      <?php } ?>
    </aside>
  </section>
</section>
