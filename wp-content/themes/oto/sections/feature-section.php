<?php
  $hero = get_sub_field('background_image');
  $size = 'full';
  $background = wp_get_attachment_image_src($hero, $size);
?>

<section class="feature-section" style="background-image: url('<?php echo $background[0]; ?>');">
  <section class="container ultra">
    <aside class="feature-section-content">
      <h1><?php the_sub_field('title'); ?></h1>
      <?php if(get_sub_field('subtitle')) { ?>
        <h3><?php the_sub_field('subtitle'); ?></h3>
      <?php } ?>
      <?php if(get_sub_field('button_url')) { ?>
        <a href="<?php the_sub_field('button_url'); ?>" class="button inverse"><?php the_sub_field('button_text'); ?></a>
      <?php } ?>
    </aside>
  </section>
</section>
