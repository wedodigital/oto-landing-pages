<section class="reviews-section">
  <section class="container ultra">
    <h2 class="section-title"><?php the_sub_field('section_title'); ?></h2>
    <?php if (get_sub_field('reviews')) { ?>
      <div class="reviews-list">
        <?php while(the_repeater_field('reviews')) { ?>
          <div class="review">
            <div class="review-body">
              <div class="star-rating star-rating-<?php the_sub_field('star_rating'); ?>">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
              </div>
              <?php the_sub_field('review'); ?>
              <p><strong><?php the_sub_field('author'); ?></strong></p>
            </div>
          </div>
        <?php } ?>
      </div>
    <?php } ?>
  </section>
</section>
