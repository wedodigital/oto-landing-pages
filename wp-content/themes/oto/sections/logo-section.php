<section class="logo-section">
  <section class="container main">
    <?php if(get_sub_field('logos')) { ?>
      <ul class="logo-list">
        <?php while(the_repeater_field('logos')) { ?>
          <?php
            $logo = get_sub_field('logo');
            $size = 'logo';
          ?>
          <li>
            <?php echo wp_get_attachment_image( $logo, $size ); ?>
          </li>
        <?php } ?>
      </ul>
    <?php } ?>
  </section>
</section>
