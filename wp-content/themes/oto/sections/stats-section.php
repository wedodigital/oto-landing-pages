<section class="stats-section">
  <section class="container ultra">
    <h2 class="section-title"><?php the_sub_field('section_title'); ?></h2>
    <?php if(get_sub_field('stats')) { ?>
      <section class="stats-roundels">
        <?php while(the_repeater_field('stats')) { ?>
          <div class="stat-roundel">
            <h4><?php the_sub_field('title'); ?></h4>
            <div class="roundel" data-percent="<?php the_sub_field('percentage'); ?>"></div>
            <p><strong><?php the_sub_field('subtitle'); ?></strong></p>
          </div>
        <?php } ?>
      </section>
    <?php } ?>
  </section>
</section>
