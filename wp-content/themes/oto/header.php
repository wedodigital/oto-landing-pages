<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap" rel="stylesheet">
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <?php get_template_part('includes/include', 'favicon'); ?>
  <title><?php bloginfo('name'); ?></title>
  <?php wp_head(); ?>
  <script type="text/javascript">
      window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"dark-bottom"};
  </script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
</head>
<body <?php body_class('wedo'); ?>>
  <header>
    <section class="banner">
      <section class="container">
        <span>Complimentary Next Day Delivery</span>
      </section>
    </section>
    <section class="main">
      <section class="container">
        <?php get_template_part('includes/content', 'logo'); ?>
      </section>
    </section>
  </header>
