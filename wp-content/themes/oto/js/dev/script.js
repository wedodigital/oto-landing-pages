var $ = jQuery.noConflict();
$(document).ready(function() {
  $('.roundel').percircle();
});

enquire
    .register("screen and (min-width:31em)", function() {
      $(window).on('scroll', function () {
        var windowHeight = $(window).height()
        var scrollTop = $(window).scrollTop(),
            footerOffset = $('footer').offset().top,
            footerDistance = (footerOffset - scrollTop)

        if (footerDistance < windowHeight) {
          $('.split-logo').addClass('visible')
        }
      });
    }, true)
    .register("screen and (max-width:31em)", function() {
        $(document).ready(function() {
          $('.logo-list, .reviews-list').bxSlider({
            pager: false,
            nextText: '<i class="fa fa-angle-right"></i>',
            prevText: '<i class="fa fa-angle-left"></i>'
          })
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
