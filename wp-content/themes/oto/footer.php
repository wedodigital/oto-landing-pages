  <footer>
    <section class="footer-main">
      <section class="container ultra">
        <div class="split-logo">
          <div class="logo-left"></div>
          <div class="logo-right"></div>
          <h3>Amplify the Silence</h3>
        </div>
        <section class="footer-columns">
          <nav>
            <h3>Quick Links</h3>
            <?php wp_nav_menu( array('theme_location' => 'footer-menu') ); ?>
          </nav>
          <nav>
            <h3>Customer Care</h3>
            <?php wp_nav_menu( array('theme_location' => 'footer-menu-2') ); ?>
          </nav>
          <div>
            <div class="payment-info">
              <h4>We accept:</h4>
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/payment-logos.png" alt="We accept VISA & MasterCard" />
            </div>
            <div class="footer-form">
              <h4>Join the OTO Community...</h4>
              <?php echo do_shortcode('[contact-form-7 id="6" title="Email signup"]'); ?>
            </div>
          </div>
        </section>
      </section>
    </section>
    <section class="footnote">
      <section class="container narrow main">
        <p class="footnote">
          These products are not for use by or sale to persons under the age of 18. These products should be used only as directed on the label. It should not be used if you are pregnant or nursing. Consult with a physician before use if you have a serious medical condition or use prescription medications. A Doctor's advice should be sought before using this and any supplemental dietary product. All trademarks and copyrights are property of their respective owners and are not affiliated with nor do they endorse this product. These statements have not been evaluated by the FDA or the MHRA. This product is not intended to diagnose, treat, cure or prevent any disease. By using this site, you agree to follow the privacy policy and all terms & conditions printed on this site. Void where prohibited by law. ©2020 OTO International Ltd
        </p>
      </section>
    </section>
  </footer>

  <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">

	<!--Google Analytics-->

	<?php wp_footer(); ?>
</body>
</html>
